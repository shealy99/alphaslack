# AlphaSlack


Pershkrimi: 

Ky eshte nje aplikacion qe lidh eventet qe ndodhin ne AlphaWeb me platformen cloud te mesazheve Slack.

Per cdo veprim qe kryhet ne alpha web, dergohet nje mesazh ne nje channel te caktuar ne workspace-in e personit te autentikuar.

Nje user mund te caktoje disa channels ku do te dergoje shitjet e AlphaWeb.


Teknologjite perdorura:

Java –Android App (Android Studio)

JavaScript – Web Application

Java enterprise Edition (Spring framework) back-end


Funksionimi: 

Krijimi i nje web serveri qe degjon gjithe kohes per inpute nga webhook-u i WebAlpha-s. 

Parsimi i objetktit te derguar prej webhookut dhe mappimi i disa fields.

Keto fields ruhen ne databazen tone PostgreSQL dhe gjithashtu dergohen si mesazhe ne cdo channel te cdo useri te loguar.


Modelimi i databazes (demo):

Tabela e shitjeve:

Metadatat: Gjithe fushat e mapuara nga objetkti json qe vjen nga webhook.

Tabela Users:

Metadatat: username, password, userId

Tabela Channels:

Metadatat: UserId, ChannelId, CreatedDate  (UserId, ChannelId --> Composite key)


User Interface:

2 aplikacione qe lidhen me databazen:
							
1. Aplikacion Web (JavaSript)

2. Aplikacion Android (Java)

Funksionaliteti eshte i njejte per te dy aplikacionet.

•	Cdo user mund te logohet ose te krijoje account te ri. 

•	Mund te shtoje ose heqe channels nga workspace i tij.

•	Te listoje gjithe eventet qe kane ndodhur, ne cdo channel qe ka konfiguruar. Qe nga momenti qe eshte krijuar channeli deri ne ato momente.

•	Cdo user qe logohet ne appin tone duhet te autentikohet gjithashtu ne workspace-in e tij ne Slack. Te autorizoje aplikacionin tone qe te beje ndryshime tek workspace i tij. Kjo behet duke ndjekur flow-n OAuth2.0 qe eshte menyra e autentikimit per API-n e Slack.

•	Flow i OAuth2 manaxhohet nga aplikacioni.


Lidhja me aplikacionin:


Webhook URL: http://77.105.196.207:8080

Hapat per tu lidhur me aplikacionin:

1. Konfigurimi i AlphaWeb webhook. Vendosja e url-s se appit tone ku presim thirrjet.

2. Krijimi i nje accounti ne Alpha Slack per te aksesuar sherbimet qe ofrojme.

3. Lidhja me nje workspace ne Slack (username, password) dhe vendosja e nje channel Id.

Pas ketij konfigurimi cdo event qe ndodh ne alpha web dergohet si mesazh ne Slack. Gjithashtu eventet listohen dhe tek aplikacioni jone: Web dhe Android.

